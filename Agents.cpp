#include "Agents.hpp"

int randInt(int min, int max)
{
    /* Return a random int from min to max.
     * Notice that the result may include both min and max.
     * i.e. return a number in [min, max].
     */
    if (max < min)
    {
        int temp = max;
        max = min;
        min = temp;
    }
    return rand() % (max - min + 1) + min;
}


Agent::Agent()
{
    setName("Virtual Agent");
}

Agent::~Agent()
{
}

void
Agent::setName(char const *name)
{
    this->m_name = name;
}

char const *
Agent::getName()
{
    return this->m_name;
}


RandomAgent::RandomAgent()
{
    setName("Random Agent");
}

RandomAgent::~RandomAgent()
{
}

/* ****************** TASK 1  Random Agent ********************* *
 * Follow the instructions to build a random agent which plays   *
 * randomly.                                                     *
 *                                                               *
 * Implement [ char RandomAgent::getAction(State gameState) ].   *
 *                                                               *
 * The implementation won't be longer than 3 lines. It should    *
 * get 64 or 128 in most cases.                                  *
 * ************************************************************* */

char
RandomAgent::getAction(State gameState)
{
    /* Get an action given a game state.
     * For Random Agent, it should randomly return a possible action in the current game state.
     * 
     * There might be some member functions of class State that are useful to you:
     *  - [ std::vector<char> State::getActions() ] This member function returns a vector of
     *    char, each char represents an action that is valid in this game state.
     * 
     * Go to 'State.cpp' for more description.
     */
    
    // YOUR CODE HERE

}


MonteCarloAgent::MonteCarloAgent()
{
    m_trial_time = 100;
    setName("Monte Carlo Agent");
}

MonteCarloAgent::~MonteCarloAgent()
{
}

void
MonteCarloAgent::setTrialTime(int trial_time)
{
    this->m_trial_time = trial_time;
}

int
MonteCarloAgent::getTrialTime()
{
    return this->m_trial_time;
}

/* ******************* TASK 2  Play a game ********************* *
 * From game state, simulate the game process and get the total  *
 * score.                                                        *
 *                                                               *
 * Implement [ int MonteCarloAgent::randomlyPlayGameOnce(State   *
 * gameState) ].                                                 *
 *                                                               *
 * Before you implement this function, you may want to take a    *
 * look at 'State.cpp'.                                          *
 * ************************************************************* */

int
MonteCarloAgent::randomlyPlayGameOnce(State gameState)
{
    /* From current game state, play the game randomly until you reach the dead state.
     * You should return the total score you got during your play.
     * 
     * HINT: You may want to use [ char RandomAgent::getAction(State gameState) ].
     */
    
    // YOUR CODE HERE

}

/* **************** TASK 3  Monte Carlo Agent ****************** *
 * Follow the instructions to build a Monte Carlo agent which    *
 * plays much better than random agent.                          *
 *                                                               *
 * Implement [ char MonteCarloAgent::getAction(State gameState) ]*
 *                                                               *
 * You should get 2048 at most time if you do the correct        *
 * implementation. See the magic AI you build by yourself!       *
 * ************************************************************* */

char
MonteCarloAgent::getAction(State gameState)
{
    /* Get an action given a game state.
     * For Monte Carlo Agent, you should
     *  1. From current game state, take a valid action.
     *  2. After taking the action, play the game randomly for many times. Here you should
     * use [ int MonteCarloAgent::getTrialTime() ] to get the trial time you need.
     *  3. Repeat step 1 for many times until you have tried all the possible actions.
     *  4. Return the action that get the highest average score during your random play.
     * 
     * HINT: You may want to use [ int MonteCarloAgent::randomlyPlayGameOnce(State gameState) ].
     * HINT: Remember to add tile after taking the action!
     */
    
    // YOUR CODE HERE
    
}

int __testRandomPlayGameOnce(State gameState)
{
    MonteCarloAgent agent;
    return agent.randomlyPlayGameOnce(gameState);
}