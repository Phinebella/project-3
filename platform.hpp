#ifndef PLATFORM_HPP_
#define PLATFORM_HPP_

#include <iostream>
#include <iomanip>
#include <cstdlib>

typedef int (*chess_type)[4];

class Game
{
public:

    Game();
    Game(int chess[4][4]);

    void RandomAdd();
    int Space();
    void Move(char arrow);
    bool isDead();
    bool isPossibleArrow(char arrow);
    chess_type GetChess();
    int GetScore();
    int GetMax();

    void print();

private:
    int BasicMove(int line[]);

    int chess[4][4] = {};
    int score;
};

#endif

#ifdef _WIN32

#include <windows.h>

#endif
